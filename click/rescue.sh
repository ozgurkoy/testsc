#!/bin/bash

if [ $# -ne "3" ]
then
 echo "enter ip,node name,hash of the click"
  exit 1
fi

echo "restarting click on $1" >> /var/log/appTester.log
echo $(date --iso-8601=seconds --utc) >> /var/log/appTester.log
echo "" >> /var/log/appTester.log
echo $(ssh root@$1 "/etc/init.d/forever -l \"$2\"") >> /var/log/appTester.log
echo "" >> /var/log/appTester.log
