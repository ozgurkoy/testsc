#!/bin/bash
REGEX="([a-zA-Z0-9\.]+?),([^,]+),([^,]+),?(.+?)"
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
for line in $(cat $DIR/list)
do
	isComm=${line:0:1}
   	if [[ $line =~ $REGEX ]] && [[ "$isComm" != "#" ]]
	then
		ip=${BASH_REMATCH[1]}
		type=${BASH_REMATCH[2]}
		command=${BASH_REMATCH[3]}
		etc=${BASH_REMATCH[4]}
		if ! [[ -z $ip ]] && ! [[ -z $type ]] &&  [[ -d "$DIR/$type" ]]
		then
			testResult=$($DIR/$type/exec.sh "$ip" "$command" "$etc")
			if [[ $testResult -ne 1 ]] 
			then
				echo "$type:$ip is not good"
				echo "$(date --iso-8601=seconds --utc)" >> /var/log/appTester.log
				echo "$ip of type $type FAILED, trying rescue" >> /var/log/appTester.log
				$("$DIR/$type/rescue.sh" "$ip" "$command" "$etc")
				echo ""
				#email here
			else
				echo "$type:$ip is good"
				echo "$(date --iso-8601=seconds --utc)" >> /var/log/appTester.log
				echo "$ip of type $type SUCCESSFUL" >> /var/log/appTester.log
				echo ""
			fi
		fi
	fi
done
