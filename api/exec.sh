#!/bin/bash
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

result=$(php $DIR/test.php $1)
if [[ $result =~ "\"code" ]]
then
echo 1
else
echo 0
fi
